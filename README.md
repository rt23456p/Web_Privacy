
### Welcome to the Base of my P2 Project!,


Welcome! Fell Free!


<br />


### About the project,

Hi, I am a 16 yr old, I am currently doing a project in school around the topic of privacy in webs and how organization can steal you information on the internet. Feel free to give me some feed back at: rt23456p@yandex.kz

### Project Purpose
An activity to inform about the different type of privacy scandals occurred in the past through the internet, such as PRISM, and Cambridge Analytica.

<br />
<br />
<br />
<br />
<br />



## All About PRISM

### How it used to happen before internet become a thing?

Throughout history, spying methods have happened before, such as in the 1990's, Air France installed mic inside first class cabin in order to benefit fellow French Company as by the request of the French intelligence agency.
<br />
Other ways of spying by intelligence agencies such as KGB, CIA happen so often that is hard to count.


### Companies that are known for selling your data
Microsoft, Yahoo, Google, Facebook, Paltalk, AOL, Skype, Apple, cisco, HSBC, CryptoAG(Non-Existent but beware) , more to be known

<br />
<br />
<br />

### Why You Don't Want to be spied by using a backdoor?
Every one should have privacy, it's a human right! Also, if you are a programmer, imagine all of you important codes! Investment? How about a internal report of a company that is going on IPO? It can get you in jail!

## Some Slides of such programs

### PRISM(Leaked by Edward Snowden in 2013) Documents(Not All)
<img src="https://user-images.githubusercontent.com/31880587/83156822-75357e00-a135-11ea-8040-53c8b04af7b9.jpg" width="25%"></img>
<img src="https://user-images.githubusercontent.com/31880587/83156829-76ff4180-a135-11ea-9c48-f0c7de07c7a6.jpg" width="25%"></img>
<img src="https://user-images.githubusercontent.com/31880587/83156831-7797d800-a135-11ea-8a85-bee6e14ebdc8.jpg" width="25%"></img>
<img src="https://user-images.githubusercontent.com/31880587/83156833-78306e80-a135-11ea-9660-e93d889e713a.jpg" width="25%"></img>
<img src="https://user-images.githubusercontent.com/31880587/83156834-78c90500-a135-11ea-8992-e74e9ab7c473.jpg" width="25%"></img>
<img src="https://gitee.com/rt23456p/source-material/raw/master/download.jpeg" width="25%"></img>




<br />

### How to be keep you data safe to avoid been tracked by PRISM?

You need a VPS
A Computer(Mac/Windows/Linux all works)

1. A solution would be to Bulid a SSR-VPN yourself with a safe VPS,([here for the backup portal](https://github.com/ToyoDAdoubiBackup/doubi)) remember not to install Google BBR algorithm as this may put you in some risks
2. Save the key and use a SSR software such as ShadowsocksR
3. Use TOR as your browser, as it can provide secure connections

#### You can also try other type of connections to a remote server such as V2Ray etc...
<br />
<br />
<br />



## Reference Section :
["Snowden Leaks Caused US 'Significant Harm' – Mueller". BBC News.](https://www.bbc.co.uk/news/world-us-canada-22884566) <br />
[Here's how the NSA spied on Cisco firewalls for years](https://www.engadget.com/2016-08-21-nsa-technique-for-cisco-spying.html) <br />
[Snowden: The NSA planted backdoors in Cisco products](https://www.infoworld.com/article/2608141/snowden--the-nsa-planted-backdoors-in-cisco-products.html) <br />
[ "Mapping the Canadian Government's Telecommunications Surveillance". citizenlab.org.](https://citizenlab.ca/2014/03/mapping-canadian-governments-telecommunications-surveillance/) <br />
[ "NSA surveillance programs live on, in case you hadn't noticed](https://www.cnet.com/news/nsa-surveillance-programs-prism-upstream-live-on-snowden/) <br />
[PRISM Slides](https://nsa.gov1.info/dni/prism.html) <br />
